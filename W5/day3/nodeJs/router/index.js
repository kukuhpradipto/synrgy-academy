const router = require('express').Router()
const homeRouter = require('./homeRouter')
const articleRouter = require('./articleRouter')


router.use('/', homeRouter)


router.use('/articles')

module.exports = router