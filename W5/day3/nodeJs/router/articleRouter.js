const router = require('express').Router()
const articleController = require('../controller/articleController')

router.get('/', articleController.index)
router.get('/detail/:id', articleController.detail)

router.get('/create', articleController.createArticle)
router.post('/create', articleController.create)
router.get('/:id', articleController.show)
router.patch('/update', articleController.update)
// router.delete('/:id', ar)

module.exports = router