const express = require('express')
const app = express()
const port = 3000

// basic middleman
app.use(express.urlencoded({extended: false}))
app.use(express.json())
app.set('view engine','ejs')


const router = require('./router/articleRouter')
app.use(router)

app.listen(port, () => console.log(`Running on your http://localhost:${port}`))