const {Article} = require('../models')

module.exports = {
    index: (req, res) => {
        Article.findAll({
        }).then(article => {
            if (article.length !== 0) {
                res.json({
                    'status': 200,
                    'message': 'sukses',
                    'data': article
                })
            }else{
                res.json({
                    'status': 400,
                    'message': 'DATA KOSONG'
                })
            }
        })
        
    },
    createArticle:(req, res) => {
        res.render('./articles/create')
    },
    detail:(req, res) => {
        Article.findOne({
            where:{
                id:req.params.id
            }
        })
        .then(article => {
            res.render('./articles/detail',{article})
        })
    },
    create: (req, res) => {
        const {
            title,
            body,
            approved
        } = req.body;

        Article.create({
            title,
            body,
            approved
        }).then(article => {
            res.json({
                'status': 200,
                'message': 'DATA BERHASIL DIMASUKKAN',
                'data': article
            })
        })
    },
    update: (req, res) => {
        const userId = req.params.id;
        const query = { 
            where: {id: userId}
        }

        Article.update({
            approved: true
        }, query).then(data => {
            res.json({
                'status': 200,
                'message': 'berhasil diupdate',
                'data': userId
            })
            // process.exit() // matikan server
        }).catch(err => {
            res.json({
                'status': 400,
                'message': 'data gagal diupdate'
            });
        })
    },
    show: (req, res) =>  {
        const userId = req.params.id;

        Article.findOne({
            where: {id: userId}
        }).then(data => {
            res.json({
                'status': 200,
                'message': 'DATA DITEMUKAN',
                'data': data
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': 'DATA TIDAK DITEMUKAN'
            });
        })
    },
    delete: (req, res) => {
        const userId = req.params.id;

        Article.destroy({
            where: {id: userId}
        }).then(data => {
            res.json({
                'status': 200,
                'message': 'DATA BERHASIL DIHAPUS',
                'data': userId
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': 'DATA GAGAL DIHAPUS'
            });
        })
    }
}