const { book } = require('./models')

book.create({
    kode_buku: 1,
    judul: 'Resep memasak',
    sinopsis: 'sinopsis',
    penulis: 'Eri Kurniawan',
    penerbit: 'Yogya penerbit',
    genre: 'memasak
})
.then(book => {
    console.log(book)
})
.catch(err => {
    console.log(err)
})